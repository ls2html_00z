#!/usr/bin/perl
#
# Makes an index.html file containing
# links to every file in the directory
#
# Modified by Abel `00z' Camarillo <00z@the00z.org> 2008/6/22

##########################################################
# User configuration variables                           #
##########################################################

$header = qq/Complete listing.../;
# Location of this (HACK.. need to find a way around this)
#$location = "/home/arkane/scripts/ls2html.pl";
$location = $0;
# font size used on headers (titles such as "files available")
$headerfontsize = 4;
# Files that are analyzed have these extensions
#@files = <*.ogg *.OGG *.mp3 *.MP3>;
@files = <*>;
# default html file written in each directory analyzed
$html = "index.html";
# Array used for list of subdirectories
@directories;

##########################################################
# Main                                                   #
##########################################################

foreach $listing (<*>) {
  if(-d $listing) {
    push(@directories, $listing);
    print("Heading into $listing\n");
    system("cd $listing && exec $location ");
  }
}

    
# Open HTML file for writing
open(FILE, ">$html") || die "Can't open file: $!\n";
select(FILE);

# Start HTML file
print("<html>\n<head>\n");
#print("<title>Files Listed</title>\n");
print("<TITLE>" . `pwd` . "</TITLE>\n");
print("</head>\n<body>");
open_table("100");
print("<TR>\n<TD ALIGN=\"center\" BGCOLOR=\"#330099\">\n");
print("<FONT SIZE=\"6\" COLOR=\"white\"><B>", $header, "</B></FONT><BR>\n");
print("</TD>\n</TR>\n");
print("<TR>\n<TD VALIGN=\"top\" BGCOLOR=\"#FFFFFF\">\n");
print("<FONT SIZE=\"$headerfontsize\"><B>Subdirectories Available</B></FONT><BR><BR>\n");
print("</TD></TR>\n");
print("</TBODY>\n");                                                                                       
print("</TABLE>\n");                                                                                       
open_table("100");
print("<TR  VALIGN=\"top\" BGCOLOR=\"#FFFFFF\">\n");
# setup counter for iteration of tables for subdirectory listing
$count = 0;
foreach $subdirectory (@directories) {
  $count++;
  print("<TD>\n");
  print("<A HREF=\"$subdirectory\">$subdirectory</A><BR>\n");
  print("</TD>\n");
  if($count == 3) {
    print("</TR>\n<TR>\n");
    $count = 0;
  }
}
print("</TR>\n");
print("</TBODY>\n");
print("</TABLE>\n");
open_table("100");
print("<TR>\n<TD VALIGN=\"top\" BGCOLOR=\"#330099\" WIDTH=\"100%\">\n");
print("<BR>\n</TD>\n</TR>\n");
print("<TR>\n<TD VALIGN=\"top\" BGCOLOR=\"#FFFFFF\" WIDTH=\"100%\">\n");
print("<FONT SIZE=\"$headerfontsize\"><B>Files Available</B></FONT>\n");
print("</TD>\n</TR>\n");
print("</TBODY>\n");                                                                                       
print("</TABLE>\n");                                                                                       
open_table("100");
print("<TR>\n");
# Iterate through files listed, href'ing each
$countagain = 0;
foreach (@files) {
  $temp1 = $_;
  $countagain++;
  print("<TD VALIGN=\"top\" BGCOLOR=\"#FFFFFF\">\n");
  # Keeps the index.html being written from showing up
  if(!/index.html/) {                        
    print("<a href=\"$temp1\">$temp1</a>\n");
  }
  # Work information out of OGG files with ogginfo
  # displaying it below the file listing                    
  if(/.ogg/i) {
    @ogginfo = `ogginfo \"$temp1\"`;
    @oggtitle = grep /title=/, @ogginfo;
    @oggtitle[0] =~ s/title=//ig; 
    @oggartistname = grep(/artist=/, @ogginfo);
    @oggartistname[0] =~ s/artist=//ig;
    @oggaveragebitrate = grep(/Average bitrate/, @ogginfo);
    @oggaveragebitrate[0] =~ s/Average bitrate://ig;
    @oggplaybacklength = grep(/Playback length/, @ogginfo);
    @oggplaybacklength[0] =~ s/Playback length//ig;
    print("<br><font size=\"3\">Title: @oggtitle &nbsp;&nbsp;&nbsp;&nbsp;\n");
    print("Artist Name: @oggartistname<br>\n");
    print("Average Bitrate: @oggaveragebitrate &nbsp;&nbsp;&nbsp;&nbsp;\n");
    print("Playback Length: @oggplaybacklength\n");
  }
  print("</TD>\n");
  if( $countagain == 2 ) {
    print("</TR>\n<TR>\n");
    $countagain = 0;
  }
}
print("</TR>\n");
print("</TBODY>\n");
print("</TABLE>");
credits();
close(FILE);

sub credits {
  print("<BR><BR><BR><FONT SIZE=\"2\"><i>\n");
  print("Automatically created with ls2html Perl Script<BR>\n");                  
  print("Copyright(c) 2002 Norman Lund (dan_lund\@hotmail.com)... and modified"
      . " by Abel `00z' Camarillo <00z\@the00z.org><BR>\n");           
  print("Script is distributed under the GNU Public License (GPL)\n");            
  print("</FONT></i>\n");                                                         
  print("</body>\n</html>\n");
}

sub open_table {
  my($mywidth) = @_;
  print("<TABLE CELLPADDING=\"2\" CELLSPACING=\"2\" BORDER=\"1\" WIDTH=\"$mywidth%\">\n");                        
  print("<TBODY>\n");                                                                     
}
