ls2html Perl script
-------------------
ls2html is a bit bad of a name, but that is basically what it does!
Place this script anywhere your heart desires, change to the top
of a directory tree that contains OGG or MP3 files, and run this script.
ls2html will traverse through the directory tree, making "index.html" files
in each directory.  The index.html files will contain HTML in table form
of the subdirectories and OGG/MP3 files contained within that directory.

This project allows you to simply run this script once, creating the HTML
files that can be used to share your MP3 and OGG file collection with the
world... without dealing with webifying or opening your webserver up to
raw directory listing.

Any ideas for improvement, or even code for improvement would be a wonderful
asset!  ls2html is a GPL script, and has been released to absorb any
improvements that the world wants!
Please see ls2html.sourceforge.net for this projects website.

Thank you,
ls2html Team
